# Unnamed modification of CataBN

Highly experimental Linux-only fork of CataBN.


## Changes so far

- Changed:
  - Records format: 1c9d9dc8, be287979, 6fac49a3, 001fd778, d03605d8;
- Removed:
  - Platform support: Android (f6a6f89c) and OSX (371bd9c6);
  - Build features: CMake (f6a6f89c), TILES=0 (28ecd7b8), SOUND=0 (b74b879);
  - Translations (f6a6f89c);


## Setup and build

Clone main repo:
```bash
git clone https://gitlab.com/somobu/catamod.git
cd catamod
```

Optionaly (but highly recommended) clone additional repos into main repo's directory:
```bash
git clone https://gitlab.com/somobu/catamod-data.git user
git clone https://gitlab.com/somobu/catamod-editor.git editor
```

After that you can build the game using one of two simple build scripts:
- `./compile.sh` just compiles `cataclysm` itself;
- `./build.sh` intended to lint-build-test everything, for now it buils game, docs and tests the game;


## Project structure

The idea of this project is to have minimal runnable and test-able version of the game in [main repo][repo_main], and to keep all the content in separate [content repo][repo_cnt]; to do so, the simple content [editor][repo_editor] was made. Docs has been moved to a mdbook, you can read it [online here][online_book].

[repo_main]: https://gitlab.com/somobu/catamod
[repo_cnt]: https://gitlab.com/somobu/catamod-data
[repo_editor]: https://gitlab.com/somobu/catamod-editor
[online_book]: https://somobu.gitlab.io/catamod

Overview of most important directories:
```
./            main repo root
  book/       mdbook with all the docs
  configs/    some dev configs
  data/       minimal game data
  doxygen/    generated reference
  editor/     editor repo root
  src/        engine source code
  user/       content repo root
```

## Metrics and TODOs

Main goal: move content piece by piece to a mods until there are only core/engine left in main repo.

Main metric: engine LOC (current - 1,175k; target - 50k).

- [ ] Secondary goal - editor
  - [ ] allow user to mass-move records
- [ ] Goal: move as many jsons out of engine repo as possible:
  - [ ] Move terrain and buildings;
  - [ ] Replace "No_XXX" mods w/ "XXX"-type mods - do not disable some data but move it to own mod;
- [ ] Goal: move mods' native code out of engine code;
    - LuaBridge + LuaJit?;
    - some kind of mod api;



## Notes

Set `use_tiles` to always-true and rm this option

Move refugee center to a mod.

Move labs to a mod.

It makes sense to find and move ids declarations from core to mods (there are many of them in files like `character.cpp`, `iuse.cpp`, `item.cpp`).


## License

Forked from [CataBN 522002b](https://github.com/cataclysmbnteam/Cataclysm-BN/tree/522002b30da138b92231c46c4c3288ca4cf19713) (14 jul 2023).

[CC SA 3.0 Unported](https://gitlab.com/somobu/catamod/-/blob/master/LICENSE.md) and others.
