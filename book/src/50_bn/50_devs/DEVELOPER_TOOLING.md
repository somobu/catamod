# Developer tooling

## Code style (astyle)

Automatic formatting of the source code is performed by [Artistic Style](http://astyle.sourceforge.net/), or `astyle` for short.

There are multiple ways to invoke it on the codebase, depending on your system or personal preferences.

### Invoking astyle directly

If you only have `astyle` installed, use:

```BASH
astyle --options=.astylerc --recursive src/*.cpp,*.h tests/*.cpp,*.h tools/*.cpp,*.h
```

### Invoking astyle through make

If you have both `make` and `astyle` installed, use:

```BASH
make astyle
```

### Invoking astyle via pre-commit hook

If you have all the relevant tools installed, you can have git automatically
check the style of code and json by adding these commands to your git
pre-commit hook (typically at `.git/hooks/pre-commit`):

```BASH
git diff --cached --name-only -z HEAD | grep -z 'data/.*\.json' | \
    xargs -r -0 -L 1 ./tools/format/json_formatter.[ce]* || exit 1

make astyle-check || exit 1
```


## JSON style

See the [JSON style guide](JSON_STYLE.md).

## ctags

In addition to the usual means of creating a `tags` file via e.g. [`ctags`](http://ctags.sourceforge.net/), we provide `tools/json_tools/cddatags.py` to augment a `tags` file with locations of definitions taken from CDDA JSON data.  `cddatags.py` is designed to safely update a tags file containing source code tags, so if you want both types of tag in your `tags` file then you can run `ctags -R . && tools/json_tools/cddatags.py`.  Alternatively, there is a rule in the `Makefile` to do this for you; just run `make ctags` or `make etags`.


## clang-tidy

Cataclysm has a [clang-tidy configuration file](../.clang-tidy) and if you have
`clang-tidy` available you can run it to perform static analysis of the
codebase.  We test with `clang-tidy` from LLVM 12.0.0 with CI, so for the most
consistent results, you might want to use that version.

To run it, you have a few options.

* `clang-tidy` ships with a wrapper script `run-clang-tidy.py`.

* To run `clang-tidy` directly try something like
```sh
grep '"file": "' build/compile_commands.json | \
    sed "s+.*$PWD/++;s+\"$++" | \
    egrep '.' | \
    xargs -P 9 -n 1 clang-tidy -quiet
```
To focus on a subset of files add their names into the `egrep` regex in the
middle of the command-line.

