# General Linux Guide

To build Cataclysm from source you will need at least a C++ compiler, some basic developer tools, and necessary build dependencies. The exact package names vary greatly from distro to distro, so this part of the guide is intended to give you higher-level understanding of the process.

## Compiler

You have three major choices here: GCC, Clang and MXE.

  * GCC is almost always the default on Linux systems so it's likely you already have it
  * Clang is usually faster than GCC, so it's worth installing if you plan to keep up with the latest experimentals
  * MXE is a cross-compiler, so of any importance only if you plan to compile for Windows on your Linux machine

(Note that your distro may have separate packages e.g. `gcc` only includes the C compiler and for C++ you'll need to install `g++`.)

Cataclysm is targeting C++14 standard and that means you'll need a compiler that supports it. You can easily check if your version of `g++` supports C++14 by running:

    $ g++ --std=c++14
    g++: fatal error: no input files
    compilation terminated.

If you get a line like:

    g++: error: unrecognized command line option ‘--std=c++14’

This means you'll need a newer version of GCC (`g++`).

The general rule is the newer the compiler the better.

## Tools

Most distros seem to package essential build tools as either a single package (Debian and derivatives have `build-essential`) or a package group (Arch has `base-devel`). You should use the above if available. Otherwise you'll at least need `make` and figure out the missing dependencies as you go (if any).

Besides the essentials you will need `git`.

If you plan on keeping up with experimentals you should also install `ccache`, which  will considerably speed-up partial builds.

## Dependencies

There are some general dependencies, optional dependencies and then specific dependencies for either curses or tiles builds. The exact package names again depend on the distro you're using, and whether your distro packages libraries and their development files separately (e.g. Debian and derivatives).

Rough list based on building on Arch:

  * General: `gcc-libs`, `glibc`, `zlib`, `bzip2`
  * Optional: `intltool`
  * Curses: `ncurses`
  * Tiles: `sdl2`, `sdl2_image`, `sdl2_ttf`, `sdl2_mixer`, `freetype2`

E.g. for curses build on Debian and derivatives you'll also need `libncurses5-dev` or `libncursesw5-dev`.

Note on optional dependencies:

  * `intltool` - for building localization files; if you plan to only use English you can skip it

You should be able to figure out what you are missing by reading the compilation errors and/or the output of `ldd` for compiled binaries.

## Make flags

Given you're building from source you have a number of choices to make:

  * `NATIVE=` - you should only care about this if you're cross-compiling
  * `RELEASE=1` - without this you'll get a debug build (see note below)
  * `LTO=1` - enables link-time optimization with GCC/Clang
  * `LANGUAGES=` - specifies localizations. See details [here](#compiling-localization-files)
  * `CLANG=1` - use Clang instead of GCC
  * `CCACHE=1` - use ccache
  * `USE_LIBCXX=1` - use libc++ instead of libstdc++ with Clang (default on OS X)

There is a couple of other possible options - feel free to read the `Makefile`.

If you have a multi-core computer you'd probably want to add `-jX` to the options, where `X` should roughly be twice the number of cores you have available.

Example: `make -j4 CLANG=1 CCACHE=1 NATIVE=linux64 RELEASE=1`

The above will build a tiles release explicitly for 64 bit Linux, using Clang and ccache and 4 parallel processes.

Example: `make -j2`

The above will build a debug-enabled curses version for the architecture you are using, using GCC and 2 parallel processes.

**Note on debug**:
You should probably always build with `RELEASE=1` unless you experience segfaults and are willing to provide stack traces.

## Compiling localization files

By default, only English language is available, and it does not require localization file.

If you want to compile files for specific languages, you should add `LANGUAGES="<lang_id_1> [lang_id_2] [...]"` option to make command:

    make LANGUAGES="zh_CN zh_TW"

You can get the language ID from the filenames of `*.po` in `lang/po` directory or use `LANGUAGES="all"` to compile all available localizations.

# Debian

Instructions for compiling on a Debian-based system. The package names here are valid for Ubuntu 12.10 and may or may not work on your system.

Building instructions, below, always assume you are running them from the Cataclysm:BN source directory.

## Linux (native) ncurses builds

Dependencies:

  * ncurses or ncursesw (for multi-byte locales)
  * build essentials

Install:

    sudo apt-get install libncurses5-dev libncursesw5-dev build-essential astyle

### Building

Run:

    make

## Linux (native) SDL builds

Dependencies:

  * SDL
  * SDL_ttf
  * freetype
  * build essentials
  * libsdl2-mixer-dev - Used if compiling with sound support.

Install:

    sudo apt-get install libsdl2-dev libsdl2-ttf-dev libsdl2-image-dev libsdl2-mixer-dev libfreetype6-dev build-essential

check correct version of SDL2 is installed by running:
```sh
> sdl2-config --version
2.0.22
```
using old version of SDL could result in [IME not working.](https://github.com/cataclysmbnteam/Cataclysm-BN/issues/1497)

### Building

A simple installation could be done by simply running:

    make

A more comprehensive alternative is:

    make -j2 RELEASE=1

The -j2 flag means it will compile with two parallel processes. It can be omitted or changed to -j4 in a more modern processor.



## Cross-compiling to linux 32-bit from linux 64-bit

Dependencies:

  * 32-bit toolchain
  * 32-bit ncursesw (compatible with both multi-byte and 8-bit locales)

Install:

    sudo apt-get install libc6-dev-i386 lib32stdc++-dev g++-multilib lib32ncursesw5-dev

### Building

Run:

    make NATIVE=linux32

## Cross-compile to Windows from Linux
To cross-compile to Windows from Linux, you will need [MXE](http://mxe.cc).

These instructions were written for Ubuntu 20.04, but should be applicable to any Debian-based environment. Please adjust all package manager instructions to match your environment.

MXE can be either installed from MXE apt repository (much faster) or compiled from source.

### Installing MXE from binary distribution

    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 86B72ED9
    sudo add-apt-repository "deb [arch=amd64] https://pkg.mxe.cc/repos/apt `lsb_release -sc` main"
    sudo apt-get update
    sudo apt-get install astyle bzip2 git make mxe-{i686,x86-64}-w64-mingw32.static-{sdl2,sdl2-ttf,sdl2-image,sdl2-mixer}

If you are not planning on building for both 32-bit and 64-bit, you might want to adjust the last apt-get invocation to install only `i686` or `x86-64` packages.

Edit your `~/.profile` as follows:

    export PLATFORM_32="/usr/lib/mxe/usr/bin/i686-w64-mingw32.static-"
    export PLATFORM_64="/usr/lib/mxe/usr/bin/x86_64-w64-mingw32.static-"

This is to ensure that the variables for the `make` command will not get reset after a power cycle.

### Installing MXE from source
Install [MXE requirements](http://mxe.cc/#requirements) and build dependencies:

    sudo apt install astyle autoconf automake autopoint bash bison bzip2 flex gettext git g++ gperf intltool libffi-dev libgdk-pixbuf2.0-dev libtool libltdl-dev libssl-dev libxml-parser-perl lzip make mingw-w64 openssl p7zip-full patch perl pkg-config python ruby scons sed unzip wget xz-utils g++-multilib libc6-dev-i386 libtool-bin

Clone MXE repo and build packages required for CBN:

    mkdir -p ~/src
    cd ~/src
    git clone https://github.com/mxe/mxe.git
    cd mxe
    make -j$((`nproc`+0)) MXE_TARGETS='x86_64-w64-mingw32.static i686-w64-mingw32.static' sdl2 sdl2_ttf sdl2_image sdl2_mixer

Building all these packages from MXE might take a while, even on a fast computer. Be patient; the `-j` flag will take advantage of all your processor cores.

If you are not planning on building for both 32-bit and 64-bit, you might want to adjust your MXE_TARGETS.

Edit your `~/.profile` as follows:

    export PLATFORM_32="~/src/mxe/usr/bin/i686-w64-mingw32.static-"
    export PLATFORM_64="~/src/mxe/usr/bin/x86_64-w64-mingw32.static-"

This is to ensure that the variables for the `make` command will not get reset after a power cycle.
### Building (SDL)

Run one of the following commands based on your targeted environment:

    make -j$((`nproc`+0)) CROSS="${PLATFORM_32}" RELEASE=1 BACKTRACE=0 PCH=0 bindist
    make -j$((`nproc`+0)) CROSS="${PLATFORM_64}" RELEASE=1 BACKTRACE=0 PCH=0 bindist


# Windows

See [COMPILING-VS-VCPKG.md](COMPILING-VS-VCPKG.md) for instructions on how to set up and use a build environment using Visual Studio on windows.

This is probably the easiest solution for someone used to working with Visual Studio and similar IDEs. -->

## Building with MSYS2

See [COMPILING-MSYS.md](COMPILING-MSYS.md) for instructions on how to set up and use a build environment using MSYS2 on windows.

MSYS2 strikes a balance between a native Windows application and a UNIX-like environment. There's some command-line tools that our project uses (notably our JSON linter) that are harder to use without a command-line environment such as what MSYS2 or CYGWIN provide.

## Building with CYGWIN

See [COMPILING-CYGWIN.md](COMPILING-CYGWIN.md) for instructions on how to set up and use a build environment using CYGWIN on windows.

CYGWIN attempts to more fully emulate a POSIX environment, to be "more unix" than MSYS2. It is a little less modern in some respects, and lacks the convenience of the MSYS2 package manager.

## Building with Clang and MinGW64

Clang by default uses MSVC on Windows, but also supports the MinGW64 library. Simply replace `CLANG=1` with `"CLANG=clang++ -target x86_64-pc-windows-gnu -pthread"` in your batch script, and make sure MinGW64 is in your path. You may also need to apply [a patch](https://sourceforge.net/p/mingw-w64/mailman/message/36386405/) to `float.h` of MinGW64 for the unit test to compile.

# BSDs

There are reports of CBN building fine on recent OpenBSD and FreeBSD machines (with appropriately recent compilers), and there is some work being done on making the `Makefile` "just work", however we're far from that and BSDs support is mostly based on user contributions. Your mileage may vary. So far essentially all testing has been on amd64, but there is no (known) reason that other architectures shouldn't work, in principle.

### Building on FreeBSD/amd64 10.1 with the system compiler

FreeBSD uses clang as the default compiler as of 10.0, and combines it with libc++ to provide C++14 support out of the box. You will however need gmake (examples for binary packages):

`pkg install gmake`

Tiles builds will also require SDL2:

`pkg install sdl2 sdl2_image sdl2_mixer sdl2_ttf`

Then you should be able to build with something like this (you can of course set CXXFLAGS and LDFLAGS in your .profile or something):

```
export CXXFLAGS="-I/usr/local/include" LDFLAGS="-L/usr/local/lib"
gmake
```

The author has not tested tiles builds, as the build VM lacks X; they do at least compile/link successfully.

### Building ncurses version on FreeBSD/amd64 9.3 with GCC 4.8.4 from ports

For ncurses build add to `Makefile`, before `VERSION`:

```Makefile
OTHERS += -D_GLIBCXX_USE_C99
CXX = g++48
CXXFLAGS += -I/usr/local/lib/gcc48/include
LDFLAGS += -rpath=/usr/local/lib/gcc48
```
Note: or you can `setenv` the above (merging `OTHERS` into `CXXFLAGS`), but you knew that.

And then build with `gmake RELEASE=1`.

### Building on OpenBSD/amd64 5.8 with GCC 4.9.2 from ports/packages

First, install g++, gmake, and libexecinfo from packages (g++ 4.8 or 4.9 should work; 4.9 has been tested):

`pkg_add g++ gmake libexecinfo`

Then you should  be able to build with something like:

`CXX=eg++ gmake`

Only an ncurses build is possible on 5.8-release, as SDL2 is broken. On recent -current or snapshots, however, you can install the SDL2 packages:

`pkg_add sdl2 sdl2-image sdl2-mixer sdl2-ttf`

and build with:

`CXX=eg++ gmake`

### Building on NetBSD/amd64 7.0RC1 with the system compiler

NetBSD has (or will have) gcc 4.8.4 as of version 7.0, which is new enough to build cataclysm. You will need to install gmake and ncursesw:

`pkgin install gmake ncursesw`

Then you should be able to build with something like this (LDFLAGS for ncurses builds are taken care of by the ncurses configuration script; you can of course set CXXFLAGS/LDFLAGS in your .profile or something):

```
export CXXFLAGS="-I/usr/pkg/include"
gmake # ncurses builds
LDFLAGS="-L/usr/pkg/lib" gmake
```

SDL builds currently compile, but did not run in my testing - not only do they segfault, but gdb segfaults when reading the debug symbols! Perhaps your mileage will vary.
