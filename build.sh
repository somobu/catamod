#!/bin/bash
set -euo pipefail

START=$(date +%s.%N)

DING() {
    cvlc -q ~/Music/system/oh_no.m4a vlc://quit
}

DIE(){
    END=$(date +%s.%N)
    DIFF=$(echo "$END $START" | awk '{printf "%.1f", $1 - $2}')

    DING

	echo $1
    echo Done in $DIFF seconds
	exit 1
}


export BACKTRACE=1
mkdir -p log

./tools/dialogue_validator.py data/json/npcs/* data/json/npcs/*/* data/json/npcs/*/*/*
# Also build chkjson (even though we're not using it), to catch any compile errors there
make -j $((`nproc`-1)) chkjson


# Code
./compile.sh            || DIE 'Build failed'
./tests/cata_test       || DIE 'Tests failed'


# Docs
echo 'Generating documentation, it will take some time'
doxygen doxygen/Doxyfile    || DIE 'Doxygen failed'


DIE 'Success'
