#!/bin/bash
set -euo pipefail

./tools/update-pch.sh

# Some kind of linting (stolen from build scripts)
# find . -name "*json" -type f -exec python3 -m json.tool {} >/dev/null \;    || DIE 'Json lint failed'
make style-all-json-parallel RELEASE=1

# Build game itself
make --silent -j$((`nproc`-1)) ASTYLE=0 PCH=1 RELEASE=1
